### Overview ###

By default transfer service is available at `http://localhost:12000/transfer`.  
The service starts with preconfigured data 
which can be found at [data.sql](src/main/resources/moneytransfer/data.sql).  
No REST API is available to add new data entries.  
Only `/transfer` operation is supported.
Monetary values with only 2-digit precision are supported. 

#### How to build ####

`mvn clean install`

to run integration tests

`mvn clean install -P integration-tests`

#### How to start ####

Launch `moneytransfer.Application` class.

### Solution Design ###

#### Threading ####

The server is configured with 4 threads for processing concurrent HTTP requests.  
Concurrent access to the same account is resolved on the storage level.  
When a lock on account can't be acquired because of another concurrent transaction
transfer operation fails straight away without waiting with status TIMEOUT.    
When a client receives status TIMEOUT it should retry transfer operation after a delay.

#### Storage ####

To stay a step close the a production system a database with communication over JDBC was chosen.  
The reasons behind the decision are persistent data safety and transaction journal keeping required for a real system for audit purposes.   
H2 in-memory database was selected just for the sake of the test.    
For guarding concurrent access to the same account no ad-hoc cache solution is used.  
The functionality relies upon locking mechanism of the underlying database.    
SELECT ... FOR UPDATE locks an entry in a table.    
The following Connection.commit() / rollback() releases the lock.

#### Data model ####
A relational data model consists of 3 tables: CLIENT, ACCOUNT and TRANSACTION.  
The corresponding SQL table definitions can be found at [tables.sql](src/main/resources/moneytransfer/tables.sql).

*  CLIENT - client data like name, surname and etc
*  ACCOUNT - a user's account. A single user could have many accounts.
*  TRANSACTION - history of transfers from one account to another. A single account could have many transaction records.

#### Transfer protocol description ####
Take a look at integration test [MoneyTransferIT](src/test/java/moneytransfer/integration/MoneyTransferIT.java).

Transfer service is available at `/transfer` relative url.  
Request and reply are in JSON format.    
Request contains 3 required fields:

*  fromAccountId - account id to transfer money from
*  toAccountId - account id to transfer money to
*  amount - amount of money to transfer

Reply has a single field:

*  status - returns one of values from [AccountService.Status](src/main/java/moneytransfer/service/AccountService.java)
