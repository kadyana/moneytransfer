package moneytransfer.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MoneyUtilsTest {

    @Test
    public void testMoneyAmount() {
        BigDecimal value = MoneyUtils.money(10.0d / 3);

        assertThat(value.toString(), is("3.33"));
        assertThat(value.scale(), is(2));
    }

    @Test
    public void testRound() {
        BigDecimal value = MoneyUtils.roundMoney(new BigDecimal(10.2));

        assertThat(value.toString(), is("10.20"));
        assertThat(value.scale(), is(2));
    }

}