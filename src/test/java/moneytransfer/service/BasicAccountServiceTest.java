package moneytransfer.service;

import com.google.common.collect.Lists;
import moneytransfer.storage.Account;
import moneytransfer.storage.MockAccountStorage;
import moneytransfer.storage.MockStorageService;
import moneytransfer.storage.MockTransactionStorage;
import moneytransfer.storage.StorageService;
import moneytransfer.storage.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import static moneytransfer.service.AccountService.Status.INVALID_AMOUNT;
import static moneytransfer.service.AccountService.Status.INVALID_FROM_ACCOUNT;
import static moneytransfer.service.AccountService.Status.INVALID_TO_ACCOUNT;
import static moneytransfer.service.AccountService.Status.NOT_ENOUGH_FUNDS;
import static moneytransfer.service.AccountService.Status.SUCCESS;
import static moneytransfer.service.AccountService.Status.TIMEOUT;
import static moneytransfer.utils.MoneyUtils.money;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

public class BasicAccountServiceTest {

    private StorageService storageService;

    private AccountService service;

    private MockAccountStorage accountStorage;

    private MockTransactionStorage transactionStorage;

    @Before
    public void doSetup() {
        storageService = new MockStorageService();

        accountStorage = new MockAccountStorage(Lists.newArrayList(
                newAccount("ACCOUNT1", money(100.0)),
                newAccount("ACCOUNT2", money(100.0))
        ));

        transactionStorage = new MockTransactionStorage();
        service = new BasicAccountService(storageService, accountStorage, transactionStorage);
    }

    @After
    public void doTeardown() {
        storageService.shutdown();
    }

    @Test
    public void testTransferSuccessful() {
        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT2",
                money(100.0));

        assertThat(status, is(SUCCESS));
        verifyAccountStored("ACCOUNT1", money(0.0));
        verifyAccountStored("ACCOUNT2", money(200.0));
    }

    @Test
    public void testSubsequentTransferSuccessful() {
        AccountService.Status status1 = service.transfer("ACCOUNT1", "ACCOUNT2",
                money(50.0));

        assertThat(status1, is(SUCCESS));
        verifyAccountStored("ACCOUNT1", money(50.0));
        verifyAccountStored("ACCOUNT2", money(150.0));

        accountStorage.releaseLocks();

        AccountService.Status status2 = service.transfer("ACCOUNT1", "ACCOUNT2",
                money(50.0));

        assertThat(status2, is(SUCCESS));
        verifyAccountStored("ACCOUNT1", money(0.0), 2);
        verifyAccountStored("ACCOUNT2", money(200.0), 2);
    }

    @Test
    public void testTransactionJournalUpdated() {
        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT2",
                money(50.0));

        assertThat(status, is(SUCCESS));
        verifyAccountStored("ACCOUNT1", money(50.0));
        verifyAccountStored("ACCOUNT2", money(150.0));

        Transaction transaction = storageService.queryInTransaction(connection ->
                transactionStorage.queryTransaction(connection, new BigDecimal(1)).get());

        assertThat(transaction.transactionId(), is(new BigDecimal(1)));
        assertThat(transaction.fromAccountId(), is("ACCOUNT1"));
        assertThat(transaction.toAccountId(), is("ACCOUNT2"));
        assertThat(transaction.amount(), is(money(50)));
    }

    @Test
    public void testTransferFailsWhenNotEnoughFunds() {
        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT2",
                money(101.0));

        assertThat(status, is(NOT_ENOUGH_FUNDS));
    }

    @Test
    public void testTransferFailsWhenTimeout() {
        storageService.runInTransaction(connection -> {
            try {
                accountStorage.queryAndLockAccount(connection, "ACCOUNT1");
            } catch (TimeoutException ex) {
                throw new RuntimeException(ex);
            }
        });

        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT2",
                money(101.0));

        assertThat(status, is(TIMEOUT));
    }

    @Test
    public void testTransferFailsWhenInvalidFromAccount() {
        AccountService.Status status = service.transfer("ACCOUNT_UNKNOWN", "ACCOUNT2",
                money(50.0));

        assertThat(status, is(INVALID_FROM_ACCOUNT));
    }

    @Test
    public void testTransferFailsWhenInvalidToAccount() {
        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT_UNKNOWN",
                money(50.0));

        assertThat(status, is(INVALID_TO_ACCOUNT));
    }

    @Test
    public void testTransferFailsWhenAmountScaleGreater2() {
        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT2",
                new BigDecimal(50.001));

        assertThat(status, is(INVALID_AMOUNT));
    }

    @Test
    public void testTransferFailsWhenNegativeAmount() {
        AccountService.Status status = service.transfer("ACCOUNT1", "ACCOUNT2",
                new BigDecimal(-10.0));

        assertThat(status, is(INVALID_AMOUNT));
    }

    private Account newAccount(String accountId, BigDecimal amount) {
        return new Account(accountId, amount, Mockito.mock(ResultSet.class));
    }

    private void verifyAccountStored(String accountId, BigDecimal expectedBalance) {
        verifyAccountStored(accountId, expectedBalance, 1);
    }

    private void verifyAccountStored(String accountId, BigDecimal expectedBalance, int expectedStoredTimes) {
        Account account = accountStorage.account(accountId);
        ResultSet resultSet = account.resultSet();
        try {
            verify(resultSet, Mockito.times(expectedStoredTimes)).updateRow();
            verify(resultSet, Mockito.times(expectedStoredTimes)).close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        assertThat(account.balance(), is(expectedBalance));
    }

}