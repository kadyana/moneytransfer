package moneytransfer.storage;

import java.math.BigDecimal;
import java.util.concurrent.TimeoutException;

public class StorageAccessor {

    private final StorageService storageService;

    private final AccountStorage accountStorage;

    public StorageAccessor() {
        this.storageService = new BasicStorageService();
        this.accountStorage = new BasicAccountStorage();
    }

    public void setAccountBalance(String accountId, BigDecimal amount) {
        storageService.runInTransaction(connection -> {
            try {
                Account account = accountStorage.queryAndLockAccount(connection, accountId).get();
                account.setBalance(amount);
                account.store();
                account.release();
            } catch (TimeoutException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public BigDecimal getAccountBalance(String accountId) {
        return storageService.queryInTransaction(connection -> {
            try {
                Account account = accountStorage.queryAndLockAccount(connection, accountId).get();
                account.release();
                return account.balance();
            } catch (TimeoutException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public void shutdown() {
        storageService.shutdown();
    }

}
