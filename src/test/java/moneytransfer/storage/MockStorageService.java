package moneytransfer.storage;

import org.mockito.Mockito;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.function.Consumer;
import java.util.function.Function;

public class MockStorageService implements StorageService {

    @Override
    public void runInTransaction(@Nonnull Consumer<Connection> callback) {
        callback.accept(mockConnection());
    }

    @Nonnull
    @Override
    public <T> T queryInTransaction(@Nonnull Function<Connection, T> callback) {
        return callback.apply(mockConnection());
    }

    @Nonnull
    @Override
    public <T> T runQuery(@Nonnull Function<Connection, T> callback) {
        return callback.apply(mockConnection());
    }

    private Connection mockConnection() {
        return Mockito.mock(Connection.class);
    }

    @Override
    public void shutdown() {
    }

}
