package moneytransfer.storage;

import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

public class MockTransactionStorage implements TransactionStorage {

    private final Map<BigDecimal, Transaction> transactionById = Maps.newHashMap();

    private int nextId = 0;

    @Override
    public void addTransaction(
            @Nonnull Connection connection,
            @Nonnull LocalDateTime transactionTime,
            @Nonnull String fromAccountId,
            @Nonnull String toAccountId,
            @Nonnull BigDecimal amount) {

        BigDecimal transactionId = new BigDecimal(++nextId);
        transactionById.put(transactionId,
                new Transaction(transactionId, fromAccountId, toAccountId, amount, LocalDateTime.now()));
    }

    @Nonnull
    @Override
    public Optional<Transaction> queryTransaction(@Nonnull Connection connection, @Nonnull BigDecimal transactionId) {
        return Optional.ofNullable(transactionById.get(transactionId));
    }

}
