package moneytransfer.storage;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public final class MockAccountStorage implements AccountStorage {

    private final Map<String, Account> accountById = Maps.newHashMap();

    private final Set<String> lockedAccountIds = Sets.newHashSet();

    public MockAccountStorage(List<Account> accounts) {
        accounts.forEach(account -> accountById.put(account.accountId(), account));
    }

    public Account account(String accountId) {
        return accountById.get(accountId);
    }

    public void releaseLocks() {
        lockedAccountIds.clear();
    }

    @Override
    public Optional<Account> queryAndLockAccount(@Nonnull Connection connection,
                                                 @Nonnull String accountId) throws TimeoutException {
        if (!lockedAccountIds.add(accountId)) {
            throw new TimeoutException("Account is locked");
        }
        Account account = accountById.get(accountId);
        return Optional.ofNullable(account);
    }

}
