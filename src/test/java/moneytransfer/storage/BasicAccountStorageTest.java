package moneytransfer.storage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertNotNull;


public class BasicAccountStorageTest {

    private StorageService storageService;

    @Before
    public void doSetup() {
        storageService = new BasicStorageService();

        new StorageInitialiser(storageService)
                .initialiseStorage();
    }

    @After
    public void doTeardown() {
        storageService.shutdown();
    }

    @Test
    public void testQueryAndLockAccountLocksDatabaseEntry() throws InterruptedException {
        AccountStorage storage = new BasicAccountStorage();
        AtomicReference<Account> accountThread1Ref = new AtomicReference<>();
        CountDownLatch firstLockedLatch = new CountDownLatch(1);
        CountDownLatch secondCompletedLatch = new CountDownLatch(1);
        Thread thread1 = new Thread(() -> storageService.runInTransaction(connection -> {
            try {
                Optional<Account> accountOpt = storage.queryAndLockAccount(connection, "ACCOUNT1");
                accountThread1Ref.set(accountOpt.get());
            } catch (TimeoutException ex) {
                throw new RuntimeException(ex);
            }

            firstLockedLatch.countDown();

            try {
                secondCompletedLatch.await(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }));
        thread1.start();

        AtomicReference<TimeoutException> exceptionRef = new AtomicReference<>();
        Thread thread2 = new Thread(() -> storageService.runInTransaction(connection -> {
            try {
                firstLockedLatch.await(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            try {
                storage.queryAndLockAccount(connection, "ACCOUNT1");
            } catch (TimeoutException ex) {
                exceptionRef.set(ex);
            }

            secondCompletedLatch.countDown();
        }));
        thread2.start();

        thread1.join(TimeUnit.SECONDS.toMillis(1));
        thread2.join(TimeUnit.SECONDS.toMillis(1));

        assertNotNull(accountThread1Ref.get());
        assertNotNull(exceptionRef.get());
    }

}