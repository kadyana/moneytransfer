package moneytransfer.storage;

import org.junit.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static moneytransfer.utils.MoneyUtils.money;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AccountTest {

    @Test
    public void testFieldsAreCorrect() {
        Account account = new Account("ACCOUNT1", money(50.0), Mockito.mock(ResultSet.class));

        assertThat(account.accountId(), is("ACCOUNT1"));
        assertThat(account.balance(), is(money(50.0)));
    }

    @Test
    public void testStoreUpdatesResultSet() throws SQLException {
        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Mockito.when(resultSet.findColumn("BALANCE")).thenReturn(2);
        Account account = new Account("ACCOUNT1", money(100.0), resultSet);

        account.store();

        verify(resultSet, times(1)).updateRow();
        verify(resultSet, times(1)).updateBigDecimal(2, money(100.0));
    }

    @Test
    public void testReleaseClosesResources() throws SQLException {
        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Statement statement = Mockito.mock(Statement.class);
        Mockito.when(resultSet.getStatement()).thenReturn(statement);
        Account account = new Account("ACCOUNT1", money(100.0), resultSet);

        account.release();

        verify(resultSet, times(1)).close();
        verify(statement, times(1)).close();
    }

}