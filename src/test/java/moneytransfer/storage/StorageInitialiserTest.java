package moneytransfer.storage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertTrue;

public class StorageInitialiserTest {

    private StorageService storageService;

    @Before
    public void doSetup() {
        storageService = new BasicStorageService();
    }

    @After
    public void doTeardown() {
        storageService.shutdown();
    }

    @Test
    public void testInitialiseStorage() {
        StorageInitialiser initialiser = new StorageInitialiser(storageService);
        initialiser.initialiseStorage();

        storageService.runQuery(connection -> {
            try(Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM CLIENT")) {
                    assertTrue(resultSet.next());
                }

                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM ACCOUNT")) {
                    assertTrue(resultSet.next());
                }

                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM TRANSACTION")) {
                    // transaction journal is empty
                }
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            return 0;
        });
    }

}