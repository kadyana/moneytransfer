package moneytransfer.storage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static moneytransfer.utils.MoneyUtils.money;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BasicTransactionStorageTest {

    private StorageService storageService;

    @Before
    public void doSetup() {
        storageService = new BasicStorageService();

        new StorageInitialiser(storageService)
                .initialiseStorage();
    }

    @After
    public void doTeardown() {
        storageService.shutdown();
    }

    @Test
    public void testAddTransaction() {
        TransactionStorage storage = new BasicTransactionStorage();
        LocalDateTime transationTime = LocalDateTime.now();

        storageService.runInTransaction(connection ->
                storage.addTransaction(connection, transationTime, "ACCOUNT1", "ACCOUNT2", money(10.0)));

        Transaction transaction = queryTransaction(BigDecimal.valueOf(1L));
        assertThat(transaction.transactionId(), is(BigDecimal.valueOf(1L)));
        assertThat(transaction.fromAccountId(), is("ACCOUNT1"));
        assertThat(transaction.toAccountId(), is("ACCOUNT2"));
        assertThat(transaction.amount(), is(money(10)));
        assertThat(transaction.time(), is(transationTime));
    }

    @Test
    public void testQueryTransaction() {
        TransactionStorage storage = new BasicTransactionStorage();
        LocalDateTime transationTime = LocalDateTime.now();
        storageService.runInTransaction(connection -> {
                storage.addTransaction(connection, transationTime, "ACCOUNT1", "ACCOUNT2", money(10.0));
                storage.addTransaction(connection, transationTime, "ACCOUNT2", "ACCOUNT1", money(20.0));
        });

        Transaction transaction = storageService.queryInTransaction(connection ->
                storage.queryTransaction(connection, BigDecimal.valueOf(1L)).get());

        assertThat(transaction.transactionId(), is(BigDecimal.valueOf(1L)));
        assertThat(transaction.fromAccountId(), is("ACCOUNT1"));
        assertThat(transaction.toAccountId(), is("ACCOUNT2"));
        assertThat(transaction.amount(), is(money(10)));
        assertThat(transaction.time(), is(transationTime));
    }

    private Transaction queryTransaction(BigDecimal transactionId) {
        return storageService.runQuery(connection -> {
            try(PreparedStatement statement =
                        connection.prepareStatement("SELECT * FROM TRANSACTION WHERE TRANSACTION_ID = ?")) {
                statement.setBigDecimal(1, transactionId);
                try(ResultSet resultSet = statement.executeQuery()) {
                    resultSet.next();
                    return Transaction.from(resultSet);
                }
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

}