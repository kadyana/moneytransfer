package moneytransfer.integration;

import moneytransfer.Application;
import moneytransfer.io.AccountServiceClient;
import moneytransfer.io.BasicHttpClient;
import moneytransfer.io.IOUtils;
import moneytransfer.storage.StorageAccessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_OK;
import static moneytransfer.utils.MoneyUtils.money;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MoneyTransferIT {

    private Application application;

    private AccountServiceClient client;
    
    private StorageAccessor storageAccessor;

    private int port;

    @Before
    public void doSetup() {
        port = IOUtils.findFreeTcpPort();
        application = new Application(port, 2);
        application.start();

        client = new AccountServiceClient(port);
        storageAccessor = new StorageAccessor();
    }

    @After
    public void doTeardown() {
        application.shutdown();        
        storageAccessor.shutdown();
    }

    /**
     * Protocol description:
     * <p>
     *
     * Request contains 3 required fields:
     * fromAccountId - account id to transfer money from
     * toAccountId - account id to transfer money to
     * amount - amount of money to transfer
     * <p>
     *
     * Reply has a single field:
     * status - returns one of values from {@link moneytransfer.service.AccountService.Status}
     *
     */
    @Test
    public void testProtocolDemo() throws IOException {
        storageAccessor.setAccountBalance("ACCOUNT1", money(100.0));
        storageAccessor.setAccountBalance("ACCOUNT2", money(100.0));

        BasicHttpClient client = new BasicHttpClient("http://localhost:" + port);

        String requestTemplate = "{'fromAccountId': '%s', 'toAccountId': '%s', amount: %s}";

        String request = String.format(requestTemplate, "ACCOUNT1", "ACCOUNT2", money(50.0));
        BasicHttpClient.Result result = client.sendPost("/transfer", request);

        assertThat(result.code(), is(HTTP_OK));

        String replyTemplate = "{'status':'%s'}";
        assertThat(result.body(), is(String.format(replyTemplate, "SUCCESS")));
    }

    @Test
    public void testTransferSuccessful() {
        storageAccessor.setAccountBalance("ACCOUNT1", money(200.0));
        storageAccessor.setAccountBalance("ACCOUNT2", money(200.0));

        AccountServiceClient.Result result = client.transfer("ACCOUNT1","ACCOUNT2", money(50.33));

        assertThat(result.code(), is(HTTP_OK));
        assertThat(result.status(), is("SUCCESS"));

        assertThat(storageAccessor.getAccountBalance("ACCOUNT1"), is(money(149.67)));
        assertThat(storageAccessor.getAccountBalance("ACCOUNT2"), is(money(250.33)));
    }

    @Test
    public void testTransferFailsWhenInvalidFromAccount() {
        storageAccessor.setAccountBalance("ACCOUNT1", money(100.0));
        storageAccessor.setAccountBalance("ACCOUNT2", money(100.0));

        AccountServiceClient.Result result = client.transfer("ACCOUNT_UNKNOWN","ACCOUNT2", money(100.0));

        assertThat(result.code(), is(HTTP_OK));
        assertThat(result.status(), is("INVALID_FROM_ACCOUNT"));

        assertThat(storageAccessor.getAccountBalance("ACCOUNT1"), is(money(100.0)));
        assertThat(storageAccessor.getAccountBalance("ACCOUNT2"), is(money(100.0)));
    }

    @Test
    public void testTransferFailsWhenInvalidToAccount() {
        storageAccessor.setAccountBalance("ACCOUNT1", money(100.0));
        storageAccessor.setAccountBalance("ACCOUNT2", money(100.0));

        AccountServiceClient.Result result = client.transfer("ACCOUNT1","ACCOUNT_UNKNOWN", money(100.0));

        assertThat(result.code(), is(HTTP_OK));
        assertThat(result.status(), is("INVALID_TO_ACCOUNT"));

        assertThat(storageAccessor.getAccountBalance("ACCOUNT1"), is(money(100.0)));
        assertThat(storageAccessor.getAccountBalance("ACCOUNT2"), is(money(100.0)));
    }

    @Test
    public void testTransferFailsWhenNotEnoughFunds() {
        storageAccessor.setAccountBalance("ACCOUNT1", money(100.0));
        storageAccessor.setAccountBalance("ACCOUNT2", money(100.0));

        AccountServiceClient.Result result = client.transfer("ACCOUNT1","ACCOUNT2", money(101.0));

        assertThat(result.code(), is(HTTP_OK));
        assertThat(result.status(), is("NOT_ENOUGH_FUNDS"));

        assertThat(storageAccessor.getAccountBalance("ACCOUNT1"), is(money(100.0)));
        assertThat(storageAccessor.getAccountBalance("ACCOUNT2"), is(money(100.0)));
    }

    @Test
    public void testTransferFailsWhenInvalidRequest() throws IOException {
        BasicHttpClient client = new BasicHttpClient("http://localhost:" + port);
        BasicHttpClient.Result result = client.sendPost("/transfer", "{ abrakadabra }");

        assertThat(result.code(), is(HTTP_BAD_REQUEST));
    }

    @Test
    public void testTransferFailsWhenMissingRequiredFields() {
        AccountServiceClient.Result result1 = client.transfer(null,"ACCOUNT2", money(101.0));

        assertThat(result1.code(), is(HTTP_BAD_REQUEST));


        AccountServiceClient.Result result2 = client.transfer("ACCOUNT1",null, money(101.0));

        assertThat(result2.code(), is(HTTP_BAD_REQUEST));


        AccountServiceClient.Result result3 = client.transfer("ACCOUNT1","ACCOUNT2", null);

        assertThat(result3.code(), is(HTTP_BAD_REQUEST));
    }

}
