package moneytransfer.io;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.IOUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class BasicHttpClient {

    private static final Logger logger = LoggerFactory.getLogger(BasicHttpClient.class);

    private final String baseUrl;

    public BasicHttpClient(@Nonnull String baseUrl) {
        Preconditions.checkArgument(StringUtils.isNotBlank(baseUrl), "baseUrl shouldn't be blank");
        this.baseUrl = baseUrl;
    }

    public Result sendPost(String servicePath, String request) throws IOException {
        URL url = new URL(baseUrl + servicePath);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        connection.setDoOutput(true);
        OutputStream out = connection.getOutputStream();
        out.write(request.getBytes());
        out.close();

        logger.info("Sent request url: {}, body: {}", url, request);

        int code = connection.getResponseCode();

        String reply;
        if (code == HttpURLConnection.HTTP_OK) {
            InputStream is = connection.getInputStream();
            reply = new String(IOUtils.readFully(is, -1, true));
            is.close();
        } else {
            reply = "";
        }

        return new Result(code, reply);
    }

    public static class Result {

        private final int code;

        private final String body;

        Result(int code, String body) {
            this.code = code;
            this.body = body;
        }

        public int code() {
            return code;
        }

        public String body() {
            return body;
        }
    }

}
