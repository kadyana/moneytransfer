package moneytransfer.io;

import java.io.IOException;
import java.net.ServerSocket;

public final class IOUtils {

    private IOUtils() {
    }

    public static int findFreeTcpPort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
