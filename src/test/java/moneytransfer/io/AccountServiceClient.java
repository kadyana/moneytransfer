package moneytransfer.io;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;

public class AccountServiceClient {

    private final BasicHttpClient client;

    public AccountServiceClient(@Nonnull String baseUrl) {
        this.client = new BasicHttpClient(baseUrl);
    }

    public AccountServiceClient(int port) {
        this("http://localhost:" + port);
    }

    public Result transfer(String fromAccountId, String toAccountId, BigDecimal amount) {
        BasicHttpClient.Result result;
        try {
            String request = "{";
            if (fromAccountId != null) {
                request += String.format("'fromAccountId':'%s'", fromAccountId);
            }
            if (toAccountId != null) {
                if (request.length() > 1) {
                    request += ",";
                }
                request += String.format("'toAccountId':'%s'", toAccountId);
            }
            if (amount != null) {
                if (request.length() > 1) {
                    request += ",";
                }
                request += String.format("'amount':%s", amount);
            }
            request += "}";
            result = client.sendPost("/transfer", request);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        String status = null;
        if (result.code() == HttpURLConnection.HTTP_OK) {
            JsonParser parser = new JsonParser();
            JsonElement root = parser.parse(result.body());
            JsonObject object = root.getAsJsonObject();
            status = object.get("status").getAsString();
        }

        return new Result(result.code(), status);
    }

    public static class Result {

        private final int code;

        private final String status;

        public Result(int code, String status) {
            this.code = code;
            this.status = status;
        }

        public int code() {
            return code;
        }

        public String status() {
            return status;
        }
    }

}
