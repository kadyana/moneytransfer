package moneytransfer.io;

import moneytransfer.service.AccountService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.HttpURLConnection;

import static moneytransfer.utils.MoneyUtils.money;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;

public class HttpServerServiceTest {

    private AccountService accountService;

    private HttpServerService service;

    private int port;


    @Before
    public void doSetup() {
        accountService = Mockito.mock(AccountService.class);
        service = null;
        port = IOUtils.findFreeTcpPort();
        service = new HttpServerService(accountService, port, 2);
        service.start();
    }

    @After
    public void doTeardown() {
        service.shutdown();
    }

    @Test
    public void testTransferHandler() {
        Mockito.when(
                accountService.transfer("ACCOUNT1", "ACCOUNT2", money(10.0)))
                .thenReturn(AccountService.Status.SUCCESS);

        AccountServiceClient client = new AccountServiceClient("http://localhost:" + port);
        client.transfer("ACCOUNT1", "ACCOUNT2", money(10.0));

        Mockito.verify(accountService, times(1))
                .transfer("ACCOUNT1", "ACCOUNT2", money(10.0));
    }

    @Test
    public void testDefaultHandler() throws IOException {
        BasicHttpClient client = new BasicHttpClient("http://localhost:" + port);

        BasicHttpClient.Result result = client.sendPost("/broken", "Garbage");

        assertThat(result.code(), is(HttpURLConnection.HTTP_NOT_FOUND));
    }

}