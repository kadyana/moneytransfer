package moneytransfer.io;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import moneytransfer.service.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.IOUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.Executors;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;

public class HttpServerService {

    private static final Logger logger = LoggerFactory.getLogger(HttpServerService.class);

    private final HttpServer server;

    private final AccountService accountService;

    private int port;

    public HttpServerService(@Nonnull AccountService accountService, int port, int nThreads) {
        Preconditions.checkArgument(port > 0, "port should be > 0");
        Preconditions.checkArgument(nThreads > 0, "nThreads should be > 0");
        this.port = port;
        this.accountService = Objects.requireNonNull(accountService, "accountService should be provided");

        try {
            server = HttpServer.create();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        server.setExecutor(Executors.newFixedThreadPool(nThreads,
                new ThreadFactoryBuilder().setNameFormat("io-http-%d").build()));

        server.createContext("/", this::handleDefault);
        server.createContext("/transfer", this::handleTransfer);
    }

    void handleDefault(HttpExchange httpExchange) throws IOException {
        logger.info("Received request, path: {}", httpExchange.getRequestURI());

        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, 0);
        httpExchange.getResponseBody().close();
    }

    void handleTransfer(HttpExchange httpExchange) throws IOException {
        String requestBody = new String(IOUtils.readFully(httpExchange.getRequestBody(), -1, true));
        logger.info("Received request, path: {}, body: {}", httpExchange.getRequestURI(), requestBody);

        TransferRequest request;
        try {
            request = new Gson().fromJson(requestBody, TransferRequest.class);
        } catch (JsonSyntaxException ex) {
            writeResponseAndClose(httpExchange, HTTP_BAD_REQUEST, "");
            return;
        }

        if (!request.validate()) {
            writeResponseAndClose(httpExchange, HTTP_BAD_REQUEST, "");
            return;
        }

        try {
            AccountService.Status status = accountService.transfer(request.fromAccountId, request.toAccountId, request.amount);
            String result = String.format("{'status':'%s'}", status.name());
            writeResponseAndClose(httpExchange, HTTP_OK, result);
        } catch (Exception ex) {
            logger.error("Unexpected exception", ex);
            writeResponseAndClose(httpExchange, HTTP_INTERNAL_ERROR, "");
        }
    }

    private void writeResponseAndClose(HttpExchange httpExchange, int httpStatus, String response) throws IOException {
        httpExchange.sendResponseHeaders(httpStatus, 0);
        OutputStream out = httpExchange.getResponseBody();
        out.write(response.getBytes());
        out.close();
    }

    public void start() {
        try {
            server.bind(new InetSocketAddress(port), 0);
            server.start();

            logger.info("Server has successfully started on port: " + port);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void shutdown() {
        server.stop(0);

        logger.info("Server has been stopped");
    }


    static class TransferRequest {

        String fromAccountId;

        String toAccountId;

        BigDecimal amount;

        boolean validate() {
            return StringUtils.isNotBlank(fromAccountId)
                    && StringUtils.isNotBlank(toAccountId)
                    && amount != null;
        }

    }

}
