package moneytransfer.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class MoneyUtils {

    public static final BigDecimal ZERO_MONEY = money(0.0);

    private MoneyUtils() {
    }

    public static BigDecimal money(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_EVEN);
    }

    public static BigDecimal roundMoney(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_EVEN);
    }

}
