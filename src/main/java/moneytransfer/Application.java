package moneytransfer;

import moneytransfer.io.HttpServerService;
import moneytransfer.service.BasicAccountService;
import moneytransfer.storage.BasicAccountStorage;
import moneytransfer.storage.BasicStorageService;
import moneytransfer.storage.BasicTransactionStorage;
import moneytransfer.storage.StorageInitialiser;
import moneytransfer.storage.StorageService;

public class Application {

    private HttpServerService httpServerService;

    private StorageService storageService;

    public Application(int port, int nThreads) {
        storageService = new BasicStorageService();

        httpServerService = new HttpServerService(
                new BasicAccountService(
                        storageService,
                        new BasicAccountStorage(),
                        new BasicTransactionStorage()),
                port, nThreads);
    }

    public void start() {
        new StorageInitialiser(storageService)
                .initialiseStorage();

        httpServerService.start();
    }

    public void shutdown() {
        httpServerService.shutdown();
        storageService.shutdown();
    }

    public static void main(String[] args) {
        new Application(12000, 4).start();
    }

}
