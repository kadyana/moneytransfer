package moneytransfer.service;

import com.google.common.base.Preconditions;
import moneytransfer.storage.Account;
import moneytransfer.storage.AccountStorage;
import moneytransfer.storage.StorageService;
import moneytransfer.storage.TransactionStorage;
import moneytransfer.utils.MoneyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

import static moneytransfer.utils.MoneyUtils.roundMoney;

public class BasicAccountService implements AccountService {

    private static final Logger logger = LoggerFactory.getLogger(BasicAccountService.class);

    private final StorageService storageService;

    private final AccountStorage accountStorage;

    private final TransactionStorage transactionStorage;

    public BasicAccountService(
            @Nonnull StorageService storageService,
            @Nonnull AccountStorage accountStorage,
            @Nonnull TransactionStorage transactionStorage) {
        this.storageService = Objects.requireNonNull(storageService, "storageService should be provided");
        this.accountStorage = Objects.requireNonNull(accountStorage, "accountStorage should be provided");
        this.transactionStorage = Objects.requireNonNull(transactionStorage, "transactionStorage should be provided");
    }

    @Nonnull
    @Override
    public Status transfer(@Nonnull String fromAccountId, @Nonnull String toAccountId, @Nonnull BigDecimal amount) {
        Preconditions.checkNotNull(fromAccountId, "fromAccountId is null");
        Preconditions.checkNotNull(toAccountId, "toAccountId is null");
        Preconditions.checkNotNull(amount, "amount is null");

        logger.info("Transfer requested from account: {}, to account: {}, amount: {}", fromAccountId, toAccountId, amount);

        if (amount.compareTo(MoneyUtils.ZERO_MONEY) < 0 || amount.scale() > 2) {
            return Status.INVALID_AMOUNT;
        }

        Status status = storageService.queryInTransaction(connection ->
                doTransfer(connection, fromAccountId, toAccountId, roundMoney(amount)));

        logger.info("Transfer completed, status: {}", status);

        return status;
    }

    private Status doTransfer(Connection connection, String fromAccountId, String toAccountId, BigDecimal amount) {
        Optional<Account> fromAccountOpt = Optional.empty();
        Optional<Account> toAccountOpt = Optional.empty();
        try {
            try {
                // Comparing account ids to prevent situation when concurrent transfers
                // account1 -> account2 and account2 -> account1 both fail due to lock taking order
                if (fromAccountId.compareTo(toAccountId) < 0) {
                    fromAccountOpt = queryAndLock(connection, fromAccountId);
                    toAccountOpt = queryAndLock(connection, toAccountId);
                } else {
                    toAccountOpt = queryAndLock(connection, toAccountId);
                    fromAccountOpt = queryAndLock(connection, fromAccountId);
                }
                if (!fromAccountOpt.isPresent()) {
                    return Status.INVALID_FROM_ACCOUNT;
                }
                if (!toAccountOpt.isPresent()) {
                    return Status.INVALID_TO_ACCOUNT;
                }
            } catch (TimeoutException e) {
                return Status.TIMEOUT;
            }

            Account fromAccount = fromAccountOpt.get();
            if (fromAccount.balance().compareTo(amount) < 0) {
                return Status.NOT_ENOUGH_FUNDS;
            }

            Account toAccount = toAccountOpt.get();
            fromAccount.setBalance(fromAccount.balance().subtract(amount));
            toAccount.setBalance(toAccount.balance().add(amount));

            fromAccount.store();
            toAccount.store();

            transactionStorage.addTransaction(connection, LocalDateTime.now(), fromAccountId, toAccountId, amount);
            return Status.SUCCESS;
        } finally {
            fromAccountOpt.ifPresent(Account::release);
            toAccountOpt.ifPresent(Account::release);
        }
    }

    private Optional<Account> queryAndLock(Connection connection, String accountId) throws TimeoutException {
        return accountStorage.queryAndLockAccount(connection, accountId);
    }

}
