package moneytransfer.service;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

public interface AccountService {

    @Nonnull
    Status transfer(@Nonnull String fromAccountId, @Nonnull String toAccountId, @Nonnull BigDecimal amount);

    enum Status {
        SUCCESS,
        NOT_ENOUGH_FUNDS,
        INVALID_FROM_ACCOUNT,
        INVALID_TO_ACCOUNT,
        INVALID_AMOUNT,
        TIMEOUT
    }

}
