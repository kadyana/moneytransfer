package moneytransfer.storage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public final class StorageInitialiser {

    private static final String TABLES_RESOURCE_NAME = "moneytransfer/tables.sql";

    private static final String DATA_RESOURCE_NAME = "moneytransfer/data.sql";

    private StorageService storageService;

    public StorageInitialiser(StorageService storageService) {
        this.storageService = storageService;
    }

    public void initialiseStorage() {
        storageService.runInTransaction(connection -> {
            applyFile(connection, TABLES_RESOURCE_NAME);
            applyFile(connection, DATA_RESOURCE_NAME);
        });
    }

    private void applyFile(Connection connection, String fileName) {
        try (Statement statement = connection.createStatement();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileName)))) {
            StringBuilder statementStr = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                statementStr.append(line);
                if (line.endsWith(";")) {
                    statement.execute(statementStr.toString());
                    statementStr.setLength(0);
                }
            }
        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
