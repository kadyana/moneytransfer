package moneytransfer.storage;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.function.Consumer;
import java.util.function.Function;

import static moneytransfer.utils.JdbcUtils.closeQuitely;

public final class BasicStorageService implements StorageService {

    private static final String DATABASE_CONNECTION_URL =
            "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;MVCC=TRUE;LOCK_TIMEOUT=0";

    public BasicStorageService() {
        initialiseDriver();
    }

    private void initialiseDriver() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DATABASE_CONNECTION_URL);
    }

    @Override
    public void runInTransaction(@Nonnull Consumer<Connection> callback) {
        queryInTransaction(connection -> {
            callback.accept(connection);
            return 0;
        });
    }

    @Nonnull
    @Override
    public <T> T queryInTransaction(@Nonnull Function<Connection, T> callback) {
        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            T result = callback.apply(connection);

            connection.commit();
            return result;
        } catch (Exception e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    RuntimeException runtimeEx = new RuntimeException(ex);
                    runtimeEx.addSuppressed(e);
                    throw runtimeEx;
                }
            }
            throw new RuntimeException(e);
        } finally {
            closeQuitely(connection);
        }
    }

    @Nonnull
    @Override
    public <T> T runQuery(@Nonnull Function<Connection, T> callback) {
        Connection connection = null;
        try {
            connection = getConnection();

            return callback.apply(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuitely(connection);
        }
    }

    @Override
    public void shutdown() {
        try {
            getConnection().createStatement().execute("SHUTDOWN");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
