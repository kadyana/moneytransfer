package moneytransfer.storage;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.function.Consumer;
import java.util.function.Function;

public interface StorageService {

    void runInTransaction(@Nonnull Consumer<Connection> callback);

    @Nonnull
    <T> T queryInTransaction(@Nonnull Function<Connection, T> callback);

    @Nonnull
    <T> T runQuery(@Nonnull Function<Connection, T> callback);

    void shutdown();

}
