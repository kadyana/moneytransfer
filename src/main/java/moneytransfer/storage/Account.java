package moneytransfer.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

import static moneytransfer.utils.JdbcUtils.closeQuietly;

public final class Account {

    private static final Logger logger = LoggerFactory.getLogger(Account.class);

    private final String accountId;

    private BigDecimal balance;

    private final ResultSet resultSet;

    Account(@Nonnull ResultSet resultSet) throws SQLException {
        this(resultSet.getString(
                resultSet.findColumn("ACCOUNT_ID")),
             resultSet.getBigDecimal(
                     resultSet.findColumn("BALANCE")),
             resultSet);
    }

    public Account(@Nonnull String accountId, @Nonnull BigDecimal balance, @Nonnull ResultSet resultSet) {
        this.accountId = Objects.requireNonNull(accountId, "accountId is null");
        this.balance = Objects.requireNonNull(balance, "balance is null");
        this.resultSet = Objects.requireNonNull(resultSet, "resultSet is null");
    }

    public String accountId() {
        return accountId;
    }

    public BigDecimal balance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public ResultSet resultSet() {
        return resultSet;
    }

    public void store() {
        try {
            int balanceColumnIndex = resultSet.findColumn("BALANCE");
            resultSet.updateBigDecimal(balanceColumnIndex, this.balance);
            resultSet.updateRow();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void release() {
        Statement statement = null;
        try {
            statement = resultSet.getStatement();
        } catch (SQLException e) {
            logger.error("Failed to get statement from resultSet", e);
        }
        closeQuietly(resultSet);
        closeQuietly(statement);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!accountId.equals(account.accountId)) return false;
        return balance.equals(account.balance);
    }

    @Override
    public int hashCode() {
        int result = accountId.hashCode();
        result = 31 * result + balance.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Account{"
                + "accountId=" + accountId
                + ", balance=" + balance
                + '}';
    }
}
