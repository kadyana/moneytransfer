package moneytransfer.storage;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.Optional;

public interface TransactionStorage {

    void addTransaction(@Nonnull Connection connection,
                        @Nonnull LocalDateTime transactionTime,
                        @Nonnull String fromAccountId,
                        @Nonnull String toAccountId,
                        @Nonnull BigDecimal amount);

    @Nonnull
    Optional<Transaction> queryTransaction(@Nonnull Connection connection, @Nonnull BigDecimal transactionId);

}
