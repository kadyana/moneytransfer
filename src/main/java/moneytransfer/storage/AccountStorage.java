package moneytransfer.storage;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

public interface AccountStorage {

    Optional<Account> queryAndLockAccount(
            @Nonnull Connection connection,
            @Nonnull String accountId) throws TimeoutException;

}
