package moneytransfer.storage;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Objects;

public final class Transaction {

    private final BigDecimal transactionId;

    private final String fromAccountId;

    private final String toAccountId;

    private final BigDecimal amount;

    private final LocalDateTime time;

    Transaction(@Nonnull BigDecimal transactionId,
                @Nonnull String fromAccountId,
                @Nonnull String toAccountId,
                @Nonnull BigDecimal amount,
                @Nonnull LocalDateTime time) {
        this.transactionId = Objects.requireNonNull(transactionId, "transactionId is null");
        this.fromAccountId = Objects.requireNonNull(fromAccountId, "fromAccountId is null");
        this.toAccountId = Objects.requireNonNull(toAccountId, "toAccountId is null");
        this.amount = Objects.requireNonNull(amount, "amount is null");
        this.time = Objects.requireNonNull(time, "time is null");
    }

    static Transaction from(ResultSet resultSet) throws SQLException {
        BigDecimal transactionId = resultSet.getBigDecimal(
                resultSet.findColumn("TRANSACTION_ID"));
        String fromAccountId = resultSet.getString(
                resultSet.findColumn("FROM_ACCOUNT_ID"));
        String toAccountId = resultSet.getString(
                resultSet.findColumn("TO_ACCOUNT_ID"));
        BigDecimal amount = resultSet.getBigDecimal(
                resultSet.findColumn("AMOUNT"));
        LocalDateTime time = resultSet.getTimestamp(
                resultSet.findColumn("TRANSACTION_TIME")).toLocalDateTime();
        return new Transaction(transactionId, fromAccountId, toAccountId, amount, time);
    }


    public BigDecimal transactionId() {
        return transactionId;
    }

    public String fromAccountId() {
        return fromAccountId;
    }

    public String toAccountId() {
        return toAccountId;
    }

    public BigDecimal amount() {
        return amount;
    }

    public LocalDateTime time() {
        return time;
    }

    @Override
    public String toString() {
        return "Transaction{"
                + "transactionId=" + transactionId
                + ", fromAccountId=" + fromAccountId
                + ", toAccountId='" + toAccountId
                + ", amount=" + amount
                + ", time=" + time
                + '}';
    }
}
