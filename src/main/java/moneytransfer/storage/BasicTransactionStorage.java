package moneytransfer.storage;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

public final class BasicTransactionStorage implements TransactionStorage {

    private static final String INSERT_TRANSACTION =
            "INSERT INTO \"TRANSACTION\"(FROM_ACCOUNT_ID, TRANSACTION_TIME, TO_ACCOUNT_ID, AMOUNT) VALUES (?, ?, ?, ?)";

    private static final String SELECT_TRANSACTION =
            "SELECT TRANSACTION_ID, TRANSACTION_TIME, FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT" +
                    " FROM \"TRANSACTION\" WHERE TRANSACTION_ID=?";


    public BasicTransactionStorage() {
    }

    @Override
    public void addTransaction(@Nonnull Connection connection,
                               @Nonnull LocalDateTime transactionTime,
                               @Nonnull String fromAccountId,
                               @Nonnull String toAccountId,
                               @Nonnull BigDecimal amount) {
        Objects.requireNonNull(connection, "connection is null");
        Objects.requireNonNull(transactionTime, "transactionTime is null");
        Objects.requireNonNull(fromAccountId, "fromAccountId is null");
        Objects.requireNonNull(toAccountId, "toAccountId is null");
        Objects.requireNonNull(amount, "amount is null");

        try(PreparedStatement statement =
                    connection.prepareStatement(INSERT_TRANSACTION)) {

            statement.setString(1, fromAccountId);
            statement.setTimestamp(2, Timestamp.valueOf(transactionTime));
            statement.setString(3, toAccountId);
            statement.setBigDecimal(4, amount);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nonnull
    @Override
    public Optional<Transaction> queryTransaction(@Nonnull Connection connection, @Nonnull BigDecimal transactionId) {
        Objects.requireNonNull(connection, "connection is null");
        Objects.requireNonNull(transactionId, "transactionId is null");

        try(PreparedStatement statement = connection.prepareStatement(SELECT_TRANSACTION)) {
            statement.setBigDecimal(1, transactionId);
            try(ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(Transaction.from(resultSet)):
                        Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
