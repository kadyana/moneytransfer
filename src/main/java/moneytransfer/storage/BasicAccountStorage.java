package moneytransfer.storage;

import org.h2.api.ErrorCode;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

import static moneytransfer.utils.JdbcUtils.closeQuietly;

public final class BasicAccountStorage implements AccountStorage {

    private static final String SELECT_AND_LOCK_ACCOUNT =
            "SELECT ACCOUNT_ID, BALANCE FROM ACCOUNT WHERE ACCOUNT_ID = ? FOR UPDATE";

    /**
     * @param connection database connection
     * @param accountId account Id
     * @return optional with account or empty when account not found.
     *         It's required to call {@link Account#release()} to free resources when account is not needed.
     * @throws TimeoutException when failed to lock account row in the database
     */
    @Override
    public Optional<Account> queryAndLockAccount(
            @Nonnull Connection connection,
            @Nonnull String accountId) throws TimeoutException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SELECT_AND_LOCK_ACCOUNT,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_UPDATABLE);
            statement.setString(1, accountId);

            resultSet = statement.executeQuery();
            return resultSet.next() ? Optional.of(new Account(resultSet)) :
                    Optional.empty();
        } catch (SQLException ex) {
            if (ex.getErrorCode() == ErrorCode.LOCK_TIMEOUT_1) {
                throw new TimeoutException(String.format("Failed to lock account entry with ACCOUNT_ID: %s", accountId));
            } else {
                throw new RuntimeException(ex);
            }
        } finally {
            closeQuietly(resultSet);
            closeQuietly(statement);
        }
    }

}
